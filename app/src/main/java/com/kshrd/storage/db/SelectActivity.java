package com.kshrd.storage.db;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.kshrd.storage.R;
import com.kshrd.storage.db.custom_adapter.MyRecyclerViewAdapter;
import com.kshrd.storage.db.data.PersonDbHelper;
import com.kshrd.storage.db.entity.Person;
import com.kshrd.storage.db.myclick.MyClickItems;

import java.util.List;

public class SelectActivity extends AppCompatActivity implements MyClickItems {
    RecyclerView recyclerView;
    List<Person> personList;
    PersonDbHelper personDbHelper;
    MyRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        personDbHelper = new PersonDbHelper(this);
        personList = personDbHelper.findAllPersons();

        recyclerView = (RecyclerView) findViewById(R.id.rvData);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        adapter = new MyRecyclerViewAdapter(personList);
        recyclerView.setAdapter(adapter);
        adapter.setListener(this);


    }

    @Override
    public void onDeleteItem(int position) {
        if (personDbHelper.deletePerson(personList.get(position).getId())) {
            adapter.removePerson(position);
            Toast.makeText(this, "Successful...!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Failed....!", Toast.LENGTH_SHORT).show();
        }

    }
}
