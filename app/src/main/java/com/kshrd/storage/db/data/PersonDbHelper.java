package com.kshrd.storage.db.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kshrd.storage.db.DbConstant;
import com.kshrd.storage.db.DbOpenHelper;
import com.kshrd.storage.db.entity.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PersonDbHelper {

    DbOpenHelper dbOpenHelper;

    public PersonDbHelper(Context context) {
        dbOpenHelper = DbOpenHelper.newInstance(context);
    }

    public List<Person> findAllPersons() {

        List<Person> personList = new ArrayList<>();

        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Cursor cursor = db.query(
                DbConstant.TABLE_PERSON,
                new String[]{DbConstant.PERSON_ID, DbConstant.PERSON_NAME, DbConstant.PERSON_GENDER},
                null,
                null,
                null,
                null,
                null
        );

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Person p = Person.getPerson(cursor);
                if (p != null) {
                    personList.add(p);
                }
                cursor.moveToNext();
            }
            return personList;
        }

        return Collections.emptyList();
    }

    public boolean addPerson(Person p) {
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbConstant.PERSON_NAME, p.getName());
        contentValues.put(DbConstant.PERSON_GENDER, p.getGender());

        long result = db.insert(DbConstant.TABLE_PERSON, null, contentValues);
        if (result > 0) {
            return true;
        }
        return false;
    }

    public boolean deletePerson(int id) {
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        long result = db.delete(DbConstant.TABLE_PERSON, DbConstant.PERSON_ID + " = ?", new String[]{String.valueOf(id)});
        if (result > 0) {
            return true;
        }
        return false;
    }

    public boolean updatePerson(Person p) {
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbConstant.PERSON_NAME, p.getName());
        contentValues.put(DbConstant.PERSON_GENDER, p.getGender());

        int result = db.update(
                DbConstant.TABLE_PERSON,
                contentValues, "id = ?",
                new String[]{String.valueOf(p.getId())}
        );

        if (result > 0) {
            return true;
        }
        return false;
    }


}
