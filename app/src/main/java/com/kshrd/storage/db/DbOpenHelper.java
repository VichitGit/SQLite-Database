package com.kshrd.storage.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by pirang on 6/7/17.
 */

public class DbOpenHelper extends SQLiteOpenHelper {

    private static DbOpenHelper dbOpenHelper;

    public static DbOpenHelper newInstance(Context context){
        if (dbOpenHelper == null){
            dbOpenHelper = new DbOpenHelper(context);
        }
        return dbOpenHelper;
    }

    private DbOpenHelper(Context context) {
        super(context, DbConstant.DB_NAME, null, DbConstant.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DbConstant.CREATE_TABLE_PERSON);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
