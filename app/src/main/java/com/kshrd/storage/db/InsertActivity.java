package com.kshrd.storage.db;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kshrd.storage.R;
import com.kshrd.storage.db.data.PersonDbHelper;
import com.kshrd.storage.db.entity.Person;

public class InsertActivity extends AppCompatActivity {

    EditText etName;
    EditText etGender;
    Button btnSubmit;

    PersonDbHelper personDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        etName = (EditText) findViewById(R.id.etName);
        etGender = (EditText) findViewById(R.id.etGender);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        personDbHelper = new PersonDbHelper(this);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Person p = new Person(etName.getText().toString(), etGender.getText().toString());
                if (personDbHelper.addPerson(p)) {
                    Toast.makeText(InsertActivity.this, "Insert Successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(InsertActivity.this, "Failed....!", Toast.LENGTH_SHORT).show();
                }
            }
        });


        Button btnSelectActivity = (Button) findViewById(R.id.btnShowData);
        btnSelectActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InsertActivity.this, SelectActivity.class);
                startActivity(intent);
            }
        });


    }
}
